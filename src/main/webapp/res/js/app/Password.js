Password = {
	requestChange : function() {
		var email = $("#emailInput").val();
		$.ajax({
			url : "api/account/forgot-password",
			method : "POST",
			data : {
				email : email
			}
		})
	},

	changePassword : function() {
		var password = $("#password").val();
		var rePassword = $("#retype-password").val();
		if (password != rePassword) {
			alert("Nhập trùng cái mày");
			return;
		}
		$.ajax({
			url : "api/account/update-password",
			method: "POST",
			data : {
				password : password
			},
			success : function(e) {
				console.log(e);
				alert(JSON.stringify(e));
			}
		})
	}
};