package vn.tcbs.cas.profile.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import vn.tcbs.cas.profile.entity.TcbsUserForgotPassword;
import vn.tcbs.cas.profile.service.PasswordService;

@Controller
@RequestMapping(value = "/")
public class PasswordController {

	@Autowired
	private PasswordService passwordService;

	@RequestMapping(value = "/doi-mat-khau", method = RequestMethod.GET)
	public ModelAndView dashboard() {
		return new ModelAndView("user/change-password");
	}

	@RequestMapping(value = "/quen-mat-khau", method = RequestMethod.GET)
	public ModelAndView forgotPassword() {
		return new ModelAndView("user/forgot-password");
	}

	@RequestMapping(value = "/xac-nhan-thay-doi-mat-khau", method = RequestMethod.GET)
	public ModelAndView forgotPasswordConfirm(HttpServletRequest req) {
		String token = req.getParameter("change_password_token");
		System.out.println(token);
		TcbsUserForgotPassword fp = passwordService.verifyToken(token);
		if (fp == null) {
			return new ModelAndView("user/forgot-password-error");
		} else {
			fp.setStatus(0);
			passwordService.increaseAccessCount(fp);
			req.getSession().setAttribute("forgot_password_user", fp.getUserId());
			return new ModelAndView("user/forgot-password-confirm");
		}
	}

	@RequestMapping(value = "/email", method = RequestMethod.GET)
	public ModelAndView preview() {
		HashMap<String, Object> hash = new HashMap<String, Object>();
		hash.put("email", "hey babe");
		return new ModelAndView("email/forgot-password", hash);
	}

}