package vn.tcbs.cas.profile.publicapi;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

public interface AccountWebService {

	public Response requestChangePassword(MultivaluedMap<String, Object> msg,
			@Context HttpServletRequest request);
}
