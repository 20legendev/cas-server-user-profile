package vn.tcbs.cas.profile.publicapi;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import vn.tcbs.cas.profile.service.PasswordService;
import vn.tcbs.tool.base.ResponseData;

@WebService
@Path(value = "/account")
public class AccountWebServiceImpl implements AccountWebService {

	@Autowired
	private PasswordService passwordService;

	private static final Logger logger = Logger
			.getLogger(AccountWebServiceImpl.class);

	@POST
	@Path("/forgot-password")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON,
			MediaType.APPLICATION_FORM_URLENCODED })
	public Response requestChangePassword(MultivaluedMap<String, Object> msg,
			@Context HttpServletRequest request) {

		ResponseData<String> res = null;
		Integer isSent = passwordService.forgotPassword((String) msg.get(
				"email").get(0));

		if (isSent == 0) {
			res = new ResponseData<String>(0, "", "SUCCESS");
		} else {
			res = new ResponseData<String>(isSent, "", "ERROR");
		}
		return Response.ok(res).build();
	}

	@POST
	@Path("/update-password")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON,
			MediaType.APPLICATION_FORM_URLENCODED })
	public Response updatePassword(MultivaluedMap<String, Object> msg,
			@Context HttpServletRequest request) {
		ResponseData<String> res = null;
		Long userId = (Long) request.getSession().getAttribute(
				"forgot_password_user");
		
		
		if (userId == null) {
			res = new ResponseData<String>(1, "", "ERROR");
		} else {
			String password = (String) msg.get("password").get(0);
			passwordService.updatePassword(userId, password);
			res = new ResponseData<String>(0, "", "SUCCESS");
		}
		return Response.ok(res).build();
	}

}