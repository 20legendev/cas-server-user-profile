package vn.tcbs.cas.profile.service;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.w3c.dom.NodeList;

import vn.tcbs.cas.profile.dao.ForgotPasswordDAO;
import vn.tcbs.cas.profile.dao.UserDAO;
import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.cas.profile.entity.TcbsUserForgotPassword;
import vn.tcbs.library.crypto.CryptoUtils;
import vn.tcbs.tool.base.TcbsSoapUtils;

import com.google.common.xml.XmlEscapers;

@Service("passwordService")
public class PasswordServiceImpl implements PasswordService {

	private static final Logger logger = Logger
			.getLogger(PasswordServiceImpl.class);

	@Value("${url.profile.forgotpassword}")
	private String forgotPasswordUrl;
	@Value("${forgotpassword.email.sender}")
	private String service;
	private String passwordSaltKey = "dont_ask_me_why_i_choose_this_one_ok_tcbs";
	@Autowired
	private VelocityEngine velocityEngine;
	@Autowired
	private ForgotPasswordDAO forgotPasswordDAO;
	@Autowired
	private UserDAO userDAO;

	public void findByEmail(String email) {
		List<TcbsUserForgotPassword> list = forgotPasswordDAO
				.findByEmail(email);
		System.out.println(list);
	}

	public Integer forgotPassword(String email) {
		try {
			List<TcbsUser> listUser = userDAO.findByEmail(email);
			if (listUser.size() == 0) {
				return 1;
			}
			String token = CryptoUtils.tokenGenerator(email);
			TcbsUser user = listUser.get(0);
			forgotPasswordDAO.addNew(user.getId(), token);
			HashMap<String, Object> model = new HashMap<String, Object>();
			model.put("email", email);
			model.put("link", forgotPasswordUrl + token);
			String emailText = VelocityEngineUtils.mergeTemplateIntoString(
					velocityEngine,
					"META-INF/velocity/email/forgot-password.vm", "utf-8",
					model);
			HashMap<String, Object> data = new HashMap<String, Object>();
			data.put("receiver", email);
			data.put("cc", "");
			data.put("subject", "[TechcomSecurities] Yêu cầu thay đổi mật khẩu");
			data.put("content",
					XmlEscapers.xmlAttributeEscaper().escape(emailText));
			data.put("fileAttach", "");
			SOAPConnection soapConnection = TcbsSoapUtils.getSoapConnection();
			SOAPMessage req = TcbsSoapUtils.generateSoapMessage(
					"soap/send-email.xml", data,
					"notification/notificationService");
			SOAPMessage soapResponse = soapConnection.call(req, this.service);
			soapConnection.close();
			NodeList nodes = soapResponse.getSOAPBody().getElementsByTagNameNS(
					"http://notification.esb.tcbs.vn/", "sendEmailResponse");

			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			soapResponse.writeTo(stream);
			String message = new String(stream.toByteArray(), "utf-8");
			System.out.println(message);
			NodeList l = nodes.item(0).getChildNodes();
			if (l.item(0).getFirstChild().getNodeValue().equals("1")) {
				return 0;
			}
			return 2;
		} catch (Exception ex) {
			ex.printStackTrace();
			return 3;
		}
	}

	@Override
	public TcbsUserForgotPassword verifyToken(String token) {
		List<TcbsUserForgotPassword> list = forgotPasswordDAO.checkToken(token);
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public void increaseAccessCount(TcbsUserForgotPassword fp) {
		fp.setAccessCount(fp.getAccessCount() + 1);
		forgotPasswordDAO.update(fp);
	}

	@Override
	public void updatePassword(Long userId, String password) {
		String passwordHash = CryptoUtils.encryptPassword(password,
				passwordSaltKey);
		userDAO.updatePassword(userId, passwordHash);
	}

}