package vn.tcbs.cas.profile.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tcbs.cas.profile.dao.UserDAO;
import vn.tcbs.cas.profile.entity.TcbsUser;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	public List<TcbsUser> findByEmail(String email) {
		return userDAO.findByEmail(email);
	}

}