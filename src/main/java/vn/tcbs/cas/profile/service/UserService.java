package vn.tcbs.cas.profile.service;

import java.util.List;

import vn.tcbs.cas.profile.entity.TcbsUser;

public interface UserService {
	public List<TcbsUser> findByEmail(String email);

}