package vn.tcbs.cas.profile.service;

import vn.tcbs.cas.profile.entity.TcbsUserForgotPassword;

public interface PasswordService {
	public Integer forgotPassword(String email);

	public TcbsUserForgotPassword verifyToken(String token);
	public void increaseAccessCount(TcbsUserForgotPassword fp);
	public void updatePassword(Long userId, String password);
}