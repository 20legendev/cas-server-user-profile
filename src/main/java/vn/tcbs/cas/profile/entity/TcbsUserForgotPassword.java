package vn.tcbs.cas.profile.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the TCBS_USER_FORGOT_PASSWORD database table.
 * 
 */
@Entity
@Table(name = "TCBS_USER_FORGOT_PASSWORD")
@NamedQuery(name = "TcbsUserForgotPassword.findAll", query = "SELECT t FROM TcbsUserForgotPassword t")
public class TcbsUserForgotPassword implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TCBS_USER_FORGOT_PASSWORD_ID_GENERATOR", sequenceName = "TCBS_FORGOT_PASSWORD_SEQUENCE")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TCBS_USER_FORGOT_PASSWORD_ID_GENERATOR")
	private long id;

	@Column(name = "ACCESS_COUNT")
	private Integer accessCount;

	@Column(name = "EXPIRED_DATE")
	private Timestamp expiredDate;

	private Integer status;

	private String token;

	@Column(name = "USER_ID")
	private Long userId;

	public TcbsUserForgotPassword() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getAccessCount() {
		return this.accessCount;
	}

	public void setAccessCount(Integer accessCount) {
		this.accessCount = accessCount;
	}

	public Timestamp getExpiredDate() {
		return this.expiredDate;
	}

	public void setExpiredDate(Timestamp expiredDate) {
		this.expiredDate = expiredDate;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getToken() {
		return this.token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}