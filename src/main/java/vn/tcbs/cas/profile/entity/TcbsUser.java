package vn.tcbs.cas.profile.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TCBS_USER database table.
 * 
 */
@Entity
@Table(name="TCBS_USER")
@NamedQuery(name="TcbsUser.findAll", query="SELECT t FROM TcbsUser t")
public class TcbsUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TCBS_USER_ID_GENERATOR", sequenceName="TCBS_USER_SEQUENCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TCBS_USER_ID_GENERATOR")
	private long id;

	private String address;

	@Temporal(TemporalType.DATE)
	private Date birthday;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	private String email;

	private String firstname;

	private BigDecimal gender;

	private String idnumber;

	private String lastname;

	private String mobile;

	private String password;

	private String phone;

	private BigDecimal relationship;

	private BigDecimal status;

	@Temporal(TemporalType.DATE)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	private String username;

	public TcbsUser() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getBirthday() {
		return this.birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public BigDecimal getGender() {
		return this.gender;
	}

	public void setGender(BigDecimal gender) {
		this.gender = gender;
	}

	public String getIdnumber() {
		return this.idnumber;
	}

	public void setIdnumber(String idnumber) {
		this.idnumber = idnumber;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public BigDecimal getRelationship() {
		return this.relationship;
	}

	public void setRelationship(BigDecimal relationship) {
		this.relationship = relationship;
	}

	public BigDecimal getStatus() {
		return this.status;
	}

	public void setStatus(BigDecimal status) {
		this.status = status;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}