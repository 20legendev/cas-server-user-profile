package vn.tcbs.cas.profile.dao;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import vn.tcbs.cas.profile.entity.TcbsUserForgotPassword;
import vn.tcbs.tool.base.BaseDAO;

@Repository("forgotPasswordDAO")
@Transactional
public class ForgotPasswordDAOImpl extends BaseDAO implements ForgotPasswordDAO {

	@Value("${forgotpassword.email.timeout}")
	private Integer emailTimeout;

	@Override
	public void addNew(TcbsUserForgotPassword fp) {
		getSession().saveOrUpdate(fp);
	}

	@Override
	public void addNew(Long userId, String token) {
		TcbsUserForgotPassword fp = new TcbsUserForgotPassword();
		fp.setToken(token);
		fp.setUserId(userId);
		fp.setStatus(1);
		fp.setAccessCount(0);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, emailTimeout);
		fp.setExpiredDate(new Timestamp(cal.getTimeInMillis()));
		getSession().save(fp);
	}

	@Override
	public void update(TcbsUserForgotPassword fp) {
		getSession().update(fp);
	}

	@Override
	public List<TcbsUserForgotPassword> findByEmail(String email) {
		// TODO Auto-generated method stub
		Criteria cr = getSession().createCriteria(TcbsUserForgotPassword.class);
		cr.createAlias("TcbsUser", "user").add(
				Restrictions.eqProperty("user.id", "userId"));
		cr.add(Restrictions.eq("user.email", email));
		return cr.list();
	}

	@Override
	public List<TcbsUserForgotPassword> checkToken(String token) {
		Criteria cr = getSession().createCriteria(TcbsUserForgotPassword.class);
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		cr.add(Restrictions.eq("token", token));
		cr.add(Restrictions.ge("expiredDate",
				new Timestamp(cal.getTimeInMillis())));
		cr.add(Restrictions.eq("status", 1));
		return cr.list();
	}

}
