package vn.tcbs.cas.profile.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import vn.tcbs.cas.profile.entity.TcbsUser;
import vn.tcbs.tool.base.BaseDAO;

@Repository("userDAO")
@Transactional
public class UserDAOImpl extends BaseDAO implements UserDAO {

	@Override
	public List<TcbsUser> findByEmail(String email) {
		Criteria cr = getSession().createCriteria(TcbsUser.class);
		cr.add(Restrictions.eq("email", email));
		return cr.list();
	}

	public TcbsUser getById(Long userId) {
		TcbsUser user = (TcbsUser) getSession().load(TcbsUser.class, userId);
		return user;
	}

	public void updatePassword(Long userId, String password) {
		TcbsUser user = getById(userId);
		if (user != null) {
			user.setPassword(password);
			getSession().update(user);
		}
	}

}
