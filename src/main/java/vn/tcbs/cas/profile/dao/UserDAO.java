package vn.tcbs.cas.profile.dao;

import java.util.List;

import vn.tcbs.cas.profile.entity.TcbsUser;


public interface UserDAO{
	public List<TcbsUser> findByEmail(String email); 
	public void updatePassword(Long userId, String password);
}
