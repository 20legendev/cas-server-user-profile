package vn.tcbs.tool.base;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;

import com.google.common.xml.XmlEscapers;

public class TcbsUtils {

	private static Pattern pattern;
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	private static final Pattern phonePattern = Pattern
			.compile("^(\\+*)\\d{10,12}$");

	public static Boolean validateEmail(String email) {
		if (pattern == null) {
			pattern = Pattern.compile(EMAIL_PATTERN);
		}
		if (email == null) {
			return false;
		}
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	public static String escapeXml(String input) {
		return XmlEscapers.xmlContentEscaper().escape(input);
	}

	public static String escapeJson(String input) {
		return StringEscapeUtils.escapeJson(input);
	}

	public static boolean validatePhone(String s) {
		if (s == null)
			return false;
		Matcher m = phonePattern.matcher(s);
		boolean isFound = m.find();
		return isFound;
	}

	public static boolean validateDate(String dateStr, String dateFormat) {
		if (dateStr == null) {
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setLenient(false);
		try {
			sdf.parse(dateStr);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}

	public static String escapeString(String input) {
		String[] arr = { "+", "-", "&&", "||", "!", "(", ")", "{", "}", "[",
				"]", "^", "\"", "~", "*", "?", ":", "\\", "AND", "OR" };

		for (int i = 0; i < arr.length; i++) {
			if (input.contains((String) arr[i])) {
				String oldString = (String) arr[i];
				String newString = new String("\\" + arr[i]);
				input = input
						.replaceAll(oldString, (String) ("\\" + newString));
			}
		}
		System.out.println(input);
		return "OK";
	}

	public static String replaceHolder(Object source,
			Map<String, Object> valueMap) {
		String staticResolved = new StrSubstitutor(valueMap).replace(source);
		Pattern p = Pattern.compile("(\\$\\{date)(.*?)(\\})");
		Matcher m = p.matcher(staticResolved);
		String dynamicResolved = staticResolved;
		while (m.find()) {
			String result = MessageFormat.format("{0,date" + m.group(2) + "}",
					new Date());
			dynamicResolved = dynamicResolved.replace(m.group(), result);
		}
		return dynamicResolved;
	}

	public static String generateRandom(int length) {
		Random random = new Random();
		char[] digits = new char[length];
		digits[0] = (char) (random.nextInt(9) + '1');
		for (int i = 1; i < length; i++) {
			digits[i] = (char) (random.nextInt(10) + '0');
		}
		return new String(digits);
	}


	public static Boolean validateLogin(HttpServletRequest req) {
		String agentId = (String) req.getSession(true).getAttribute("agentid");
		if (agentId == null || agentId.equals("")) {
			return false;
		}
		return true;
	}

	public static String trim(String input){
		if(input == null){
			return null;
		}
		input = input.replaceAll("( )+", " ");
		return input;
	}

}